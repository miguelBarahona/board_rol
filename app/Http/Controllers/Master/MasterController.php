<?php
namespace App\Http\Controllers\Master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Board;

class MasterController extends Controller
{
    public function sendMist(Request $request){   
        $mist = Board::select('mist')->where('id_board','1')->get();

        $old_mist = Board::find(1);
        $old_mist->width = $request -> width;
        $old_mist->height = $request -> height;
        $old_mist->save();

        $board = Board::updateOrCreate(
            ['id_board' => 1],
            ['mist' => $request->mist_json]
        );
        
    }
}
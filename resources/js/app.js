/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.eventControl = new Vue({})
window.eventBusMaster = new Vue({})
window.eventPlayer = new Vue({});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('menu-master-component', require('./components/Master/menuMasterComponent.vue').default);
Vue.component('logo-master-component', require('./components/Master/logoMasterComponent.vue').default);
Vue.component('select-adventure-master-component', require('./components/Master/selectAdventureMasterComponent.vue').default);
Vue.component('board-master-component', require('./components/Master/boardMasterComponent.vue').default);
Vue.component('select-map-component', require('./components/Master/selectMapComponent.vue').default);

Vue.component('menu-player-component', require('./components/Player/menuPlayerComponent.vue').default);
Vue.component('logo-player-component', require('./components/Player/logoPlayerComponent.vue').default);
Vue.component('board-player-component', require('./components/Player/boardPlayerComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: { 
        menu : 0,
        url:'http://127.0.0.1:8000'
    },
});

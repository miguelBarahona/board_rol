<html lang="es" style="height:100%;width:100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <title>@yield('title')</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" style="height:100%;width:100%;">  
    <div id='app' style="bg-warning; height: 100%;">    
        <!-- Contenido Principal -->               
        <menu-player-component :urlserver = "url"></menu-player-component>
        <!-- /Fin del contenido principal -->

        <!-- Menu de opciones derecho -->
    </div> 
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>   
</body>

</html>

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\BoardObserver;
use App\Board;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Board::observe(BoardObserver::class);
    }
}

<?php
namespace App\Http\Controllers\Player;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Board;

class PlayerController extends Controller
{
    public function getMist(Request $request){   
        $mist = Board::select('*')->where('id_board','1')->get();
        return json_encode($mist);
    }
}
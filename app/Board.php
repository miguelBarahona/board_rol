<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $primaryKey = 'id_board';
    public $timestamps = false;
    protected $fillable = [
    'id_board', 
    'old_mist',
    'mist', 
    'width',
    'height'
    ];
}
